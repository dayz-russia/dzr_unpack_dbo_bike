class ActionUnpackDboBikeCB : ActionContinuousBaseCB
{
	override void CreateActionComponent()
	{
		
		//ref Param2<int, int> m_Config = GetDayZGame().DZRUDB_ReadServerConfig();
		m_ActionData.m_ActionComponent = new CAContinuousTime(3);
	}
};

class ActionUnpackDboBike: ActionContinuousBase
{
	
	
	ref protected EffectSound m_UnpackingLoopSound;
	protected string m_UnpackingBSoundset;
	
	void ActionUnpackDboBike()
	{
		m_CallbackClass = ActionUnpackDboBikeCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_INTERACT;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_CROUCH;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_LOW;
		
		m_UnpackingBSoundset 	= "Unpack_Bicycle_start"; //Samples mixed from Zapsplat.com
		
	}
	
	void ~ActionUnpackDboBike()
	{
		DestroyTearingLoopSound();
	}
	
	override void CreateConditionComponents()
	{
		m_ConditionItem = new CCINonRuined;
		m_ConditionTarget = new CCTNone;
	}
	
	override bool HasTarget()
	{
		return false;
	}
	
	override string GetText()
	{
		return "#STR_UnpackDboBike";
	}
	override void OnStartAnimationLoopClient( ActionData action_data )
	{
		super.OnStartAnimationLoopClient(action_data);
		
		PlayTearingLoopSound( action_data );
	}
	
	override void OnEndAnimationLoopClient( ActionData action_data )
	{
		super.OnEndAnimationLoopClient(action_data);
		
		StopTearingLoopSound();
	}
	
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( item.IsKindOf( "DZR_PackedBicycle" )
			{
				return true;
			}
			return false;
		}
		
		
		void PlayTearingLoopSound( ActionData action_data )
		{
			if ( GetGame().IsMultiplayer() && GetGame().IsClient() || !GetGame().IsMultiplayer() )
			{
				if ( !m_UnpackingLoopSound )
				{
					m_UnpackingLoopSound = new EffectSound;
				}
				if ( !m_UnpackingLoopSound.IsSoundPlaying() )
				{
					m_UnpackingLoopSound = SEffectManager.PlaySound( m_UnpackingBSoundset, action_data.m_Player.GetPosition(),0,0,false );
					m_UnpackingLoopSound.SetSoundAutodestroy( true );
				}
			}
		}
		
		void StopTearingLoopSound()
		{
			if ( m_UnpackingLoopSound )
			{
				m_UnpackingLoopSound.SetSoundFadeOut(3);
				m_UnpackingLoopSound.SoundStop();
			}
		}
		
		
		void DestroyTearingLoopSound()
		{
			if ( m_UnpackingLoopSound )
			{
				SEffectManager.DestroySound( m_UnpackingLoopSound );
			}
		}
		
		
		override void OnFinishProgressServer( ActionData action_data )
		{	
			
			string ItemName = "dbo_Bicycle"; //dbo_Bicycle Offroad_02
			EntityAI m_ResultingItem = action_data.m_Player.SpawnEntityOnGroundPos(ItemName, action_data.m_Player.GetPosition() + action_data.m_Player.GetDirection() * 1 ); 
			
			//Taken from Community Online Tools by Jacob Mungo
			array< string > attachments = new array< string >;
			
			if(ItemName == "Offroad_02")
			{
				attachments.Insert("Offroad_02_Hood");
				attachments.Insert("Offroad_02_Trunk");
				attachments.Insert("Offroad_02_Door_1_1");
				attachments.Insert("Offroad_02_Door_1_2");
				attachments.Insert("Offroad_02_Door_2_1");
				attachments.Insert("Offroad_02_Door_2_2");
				attachments.Insert("Offroad_02_Wheel");
				attachments.Insert("Offroad_02_Wheel");
				attachments.Insert("Offroad_02_Wheel");
				attachments.Insert("Offroad_02_Wheel");
				attachments.Insert("CarBattery");
				attachments.Insert("GlowPlug");
				attachments.Insert("HeadlightH7");
				attachments.Insert("HeadlightH7");
			}
			else
			{
				attachments.Insert("Battery_Bike");
				attachments.Insert("bikeWheelD");
				attachments.Insert("bikeWheelD");
				attachments.Insert("bike_reargear");
				attachments.Insert("bike_frontdisc");
				attachments.Insert("HeadlightH7");
			}
			
			for ( int j = 0; j < attachments.Count(); j++ )
			{
				m_ResultingItem.GetInventory().CreateInInventory( attachments[j] );
			}
			
			Car car;
			if (Class.CastTo(car, m_ResultingItem))
			{
				float cap = car.GetFluidCapacity( CarFluid.FUEL );
				car.Fill( CarFluid.FUEL, cap );
				
				cap = car.GetFluidCapacity( CarFluid.OIL );
				car.Fill( CarFluid.OIL, cap );
				
				cap = car.GetFluidCapacity( CarFluid.BRAKE );
				car.Fill( CarFluid.BRAKE, cap );
				
				cap = car.GetFluidCapacity( CarFluid.COOLANT );
				car.Fill( CarFluid.COOLANT, cap );
			}
			//Taken from Community Online Tools by Jacob Mungo
			
			//m_ResultingItem.SetLifetime(3888000);
			
			EntityAI inHands = action_data.m_Player.GetHumanInventory().GetEntityInHands();
			action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
			inHands.Delete();
		}
		
		
		
	};
	
